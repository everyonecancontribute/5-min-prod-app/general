# 5minprod.app Collaboration Group

This is the organisation tracker for TODOs.

## Resources

- [Issue Board](https://gitlab.com/everyonecancontribute/keptn/general/-/boards/2426322)
- [Agenda](https://docs.google.com/document/d/1hjKGeOzqdcAnjpWv4R9YejyDP4lc75sqcBW3e501g8U/edit?usp=sharing) (internal)
- [DE tracking issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4121)

### Upstream

- [Deployment template](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template)
- [Examples](https://gitlab.com/gitlab-org/5-minute-production-app/examples)

## Workflow

- Create new issues using the [new-task](https://gitlab.com/everyonecancontribute/5-min-prod-app/general/-/issues/new?issuable_template=new-task) template.
- Use the `group::5-min-prod-app` label for new issues. This is applied automatically in the `new-task` template.


